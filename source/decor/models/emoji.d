module decor.models.emoji;

import vibe.data.json;
import vibe.data.serialization;
import std.typecons : Nullable;

import decor.models.role;
import decor.models.user;

struct Emoji
{
    Nullable!string id;
    Nullable!string name;
    @optional Role[] roles;
    @optional PartialUser user;
    @optional bool managed;
    @optional bool animated;
    @optional bool available;
}