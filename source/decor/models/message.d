module decor.models.message;

import std.typecons : Nullable;
import vibe.data.json;
import vibe.data.serialization;

import decor.lowlevel.types;
import decor.models.user;


struct Message
{
    string id;
    string channel_id;
    @optional Nullable!string guild_id;
    PartialUser author;
    string content;
    string timestamp;
    //@embedNullable Nullable!string edited_timestamp;
    bool mention_everyone;
    PartialUser[] mentions;
    bool pinned;
}